import {NgModule} from '@angular/core';

import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {MatNativeDateModule} from '@angular/material/core';
import {MatIconModule} from '@angular/material/icon';
import {MatBadgeModule} from '@angular/material/badge';
import {MatCardModule} from '@angular/material/card';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HeaderComponent} from './template/header/header.component';
import {MainComponent} from './pages/main/main.component';
import {AuthComponent} from './pages/auth/auth.component';
import {RegisterComponent} from './pages/auth/register/register.component';
import {LoginComponent} from './pages/auth/login/login.component';
import {ProductsComponent} from './pages/products/products.component';
import {ProductComponent} from './pages/product/product.component';
import {ProfileComponent} from './pages/profile/profile.component';
import {SliderComponent} from './pages/main/slider/slider.component';
import {SlideComponent} from './pages/main/slider/slide/slide.component';
import {ProductItemComponent} from './components/product-item/product-item.component';
import {ProductCarouselComponent} from './components/product-carousel/product-carousel.component';
import { CartComponent } from './pages/cart/cart.component';

@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        MainComponent,
        AuthComponent,
        RegisterComponent,
        LoginComponent,
        ProductsComponent,
        ProductComponent,
        ProfileComponent,
        SliderComponent,
        SlideComponent,
        ProductItemComponent,
        ProductCarouselComponent,
        CartComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        MatNativeDateModule,
        MatIconModule,
        MatBadgeModule,
        MatCardModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
