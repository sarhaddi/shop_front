import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-product-carousel',
    templateUrl: './product-carousel.component.html',
    styleUrls: ['./product-carousel.component.css']
})
export class ProductCarouselComponent implements OnInit {
    index: number = 0;

    constructor() {
    }

    ngOnInit(): void {
    }

    next() {
        if (this.index >= 4) {
            this.index = 4;
            return;
        }
        this.index++;
    }

    prev() {
        if (this.index <= 0) {
            this.index = 0;
            return;
        }
        this.index--;
    }
}
