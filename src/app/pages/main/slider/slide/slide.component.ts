import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-slide',
  templateUrl: './slide.component.html',
  styleUrls: ['./slide.component.css']
})
export class SlideComponent implements OnInit {
  @Input() image: string | undefined;
  @Input() productName: string | undefined;
  @Input() productCat: string | undefined;
  @Input() productPrice: number = 0;
  @Input() active: Boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

}
