import {Component, OnDestroy, OnInit} from '@angular/core';
import {interval, Subscription} from 'rxjs';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.css']
})
export class SliderComponent implements OnInit, OnDestroy {
  index: number = 0;
  indexSub: Subscription | undefined;

  constructor() { }

  ngOnInit(): void {
    this.runIndexInterval()
  }

  ngOnDestroy() {
    if (this.indexSub) {
      this.indexSub.unsubscribe();
    }
  }

  runIndexInterval() {
    if (this.indexSub) {
      this.indexSub.unsubscribe();
    }
    this.indexSub = interval(5000).subscribe(() => {
      this.index++;
    });
  }

  changeIndex(i: number) {
    this.index = i;
    this.runIndexInterval();
  }
}
