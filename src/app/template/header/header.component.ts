import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  collapse: number = 0;

  constructor() { }

  ngOnInit(): void {
  }

  collapseToggle() {
    switch (this.collapse) {
      default: return;
      case 0:
        this.collapse = -1;
        setTimeout(() => this.collapse = 1,500);
        break;
      case 1:
        this.collapse = -1;
        setTimeout(() => this.collapse = 0,500);
        break;
    }
  }

}
